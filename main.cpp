#include "accumulate_water_iterative.h"
#include "accumulate_water_recursive.h"

#include <random>

template<typename T>
auto get_random_input(size_t size) {
    auto result = std::vector<T>();
    result.reserve(size);
    static thread_local auto rd = std::random_device{};
    auto gen = std::mt19937(rd());
    std::generate_n(std::back_inserter(result), size, [&gen]() mutable { return std::uniform_int_distribution(1, 6)(gen); });
    return result;
}

#include <chrono>
#include <iostream>
#include <functional>
#include <vector>

int main() {
    auto input = get_random_input<size_t>(50);

    // auto input = std::vector{2, 6, 3, 5, 2, 8, 1, 4, 2, 2, 5, 3, 5, 7, 4, 1};
    // auto input = std::vector{1, 5, 3, 7, 2};
    // auto input = std::vector{5, 3, 7, 2, 6, 4, 5, 9, 1, 2};
    // auto input = std::vector{6, 7, 10, 7, 6};
    // auto input = std::vector{5, 5, 5, 5};
    // auto input = std::vector{5, 6, 7, 8};
    // auto input = std::vector{8, 7, 7, 6};
    constexpr auto less = std::less<typename decltype(input)::value_type>{};

    using Clock = std::chrono::steady_clock;
    auto const start0 = Clock::now();
    auto const result1 = accumulate_water_recursive<int>(begin(input), end(input), less);
    auto const duration0 = Clock::now() - start0;
    auto const start1 = Clock::now();
    auto const result0 = accumulate_water_iterative<typename decltype(input)::value_type>(begin(input), end(input), less);
    auto const duration1 = Clock::now() - start1;
    std::cout << result0 << " units of water\n";
    std::cout << result1 << " units of water\n";
    auto constexpr toInteger = [](auto d) { return std::chrono::duration_cast<std::chrono::microseconds>(d).count(); };
    std::cout << "\nTook " << toInteger(duration0) 
            << " vs. " << toInteger(duration1) 
            << " us\n";
}
