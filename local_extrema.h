#pragma once

#include <iterator>
#include <numeric>
#include <utility>

// NB parameter `to` is in the range, NOT past the end
template <typename Init, typename Iterator0, typename Iterator1>
Init accumulate_balance(Iterator0 from, Iterator1 to, Init init={}) {
    using std::distance;
    auto const size = distance(from, to);
    if (size < 2) { return init; }
    auto const height = std::min(*from, *to);
    return std::accumulate(++from, to, init, [height](auto lhs, auto rhs) { return lhs + height - rhs; });
}

template <typename Iterator0, typename Iterator1, typename Op>
auto find_first_consecutive_comp(Iterator0 from, Iterator1 to, Op less) {
    if (from == to) { return from; }
    for (auto prev = from++; from != to; ++from, ++prev) {
        if (less(*prev, *from)) { return prev; }
    }
    return from;
}

template <typename Iterator0, typename Iterator1, typename Op>
auto next_local_extremum(Iterator0 from, Iterator1 to, Op less) {
    constexpr auto grtr = [less](auto&& lhs, auto&& rhs) { return less(std::forward<decltype(rhs)>(rhs), std::forward<decltype(lhs)>(lhs)); };
    
    auto extreme = find_first_consecutive_comp(from, to, less);
    if (extreme == to) {  // there is a local minimum on the border
        return extreme; 
    }
    extreme = find_first_consecutive_comp(extreme, to, grtr);
    return extreme == to ? --extreme : extreme;  // special case: extremum on border
}
