cmake_minimum_required(VERSION 3.10)

project(water-towers-2d VERSION 1.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_executable(water main.cpp)
