#pragma once

#include "local_extrema.h"

// if front is the extremum already, return the largest local extremum
template <typename Iterator0, typename Iterator1, typename Op>
auto find_first_comp_front(Iterator0 const from, Iterator1 const to, Op less) {
    if (from == to) { return from; }
    
    auto extreme = next_local_extremum(from, to, less);
    if (extreme == to) { 
        return extreme; 
    }

    if (less(*from, *extreme)) {
        return extreme;
    }

    for (auto it = next_local_extremum(extreme+1, to, less); it != to; it = next_local_extremum(it+1, to, less)) {
        if (less(*extreme, *it)) {
            extreme = it;
            if (less(*from, *it)) {
                return it;
            }
        }
    }
    return extreme;
}

template <typename Init, typename Iterator0, typename Iterator1, typename Op>
Init accumulate_water_iterative(Iterator0 from, Iterator1 const past, Op less, Init init={}) {
    constexpr auto grtr = [less](auto&& lhs, auto&& rhs) { return less(std::forward<decltype(rhs)>(rhs), std::forward<decltype(lhs)>(lhs)); };
    from = find_first_consecutive_comp(from, past, grtr);
    for (auto to = find_first_comp_front(from, past, less);
        to != past;
        from = find_first_consecutive_comp(to, past, grtr), to = find_first_comp_front(from, past, less))
    {
        init = accumulate_balance(from, to, init);
    }
    return init;
}
