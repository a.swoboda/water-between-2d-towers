#pragma once

#include "local_extrema.h"

#include <algorithm>

template <typename Init, typename Iterator0, typename Iterator1, typename Op>
Init accumulate_water_stable_sorted_extremes(Iterator0 const fromSorted, Iterator1 const toSorted, Op less, Init init = {}) {
    using std::distance;
    if (distance(fromSorted, toSorted) < 2) { 
        return init; 
    }
    auto less0 = std::less<std::remove_reference_t<decltype(*fromSorted)>>();
    auto less1 = [less](auto lhs, auto rhs) { return less(*lhs, *rhs); };
    // get the first candidates
    auto start = std::lower_bound(fromSorted, toSorted, *(toSorted-2), less1);
    std::sort(start, toSorted, less0);
    init = accumulate_balance(*start, *(toSorted-1), init);
    // remove extrema in between
    auto middle = std::stable_partition(fromSorted, start, [less0, border=*start](auto it) { return less0(it, border); });
    std::iter_swap(middle++, start++);
    start = std::stable_partition(middle, start, [less0, border=*(toSorted-1)](auto it) { return less0(border, it); });
    std::iter_swap(start++, toSorted-1);
    // recurse 
    init = accumulate_water_stable_sorted_extremes(fromSorted, middle, less, init);
    init = accumulate_water_stable_sorted_extremes(middle, start, less, init);
    return init;
}

#include <vector>

template <typename Init, typename Iterator0, typename Iterator1, typename Op>
Init accumulate_water_recursive(Iterator0 const from, Iterator1 const to, Op less, Init init = {}) {;
    using value_type = Iterator0;
    auto lookup = std::vector<value_type>{};
    constexpr auto grtr = [less](auto&& lhs, auto&& rhs) { return less(std::forward<decltype(rhs)>(rhs), std::forward<decltype(lhs)>(lhs)); };
    for (auto it=find_first_consecutive_comp(from, to, grtr); it != to; it=next_local_extremum(it, to, less)) {
        lookup.emplace_back(it);
    }
    std::stable_sort(begin(lookup), end(lookup), [less](auto&& lhs, auto&& rhs) { return less(*lhs, *rhs); });
    return accumulate_water_stable_sorted_extremes(begin(lookup), end(lookup), less, init);
}
